from flask import Flask, render_template, request

app = Flask(__name__)

#The routes commented out are not in use.  I left them there in case
#I need them for future projects. Their subsequent functions, however,
#are in use by the 'parseboi' function.

#@app.route('/trivia.html')
def trivhtml():
    return render_template('trivia.html'), 200

#@app.route('/trivia.css')
def trivcss():
    return render_template('trivia.css'), 200

#@app.errorhandler(403)
def error_403(error):
    return render_template('403.html'), 403

@app.errorhandler(404)
def error_404(error):
    return render_template('404.html'), 404

@app.route('/<path:request>')
def parseboi(request):

    rvalue = error_404(404)

    if '//' in request:
        rvalue = error_403(403)
    elif '~' in request:
        rvalue = error_403(403)
    elif '..' in request:
        rvalue = error_403(403)

    elif request.endswith('.html') or request.endswith('.css'):
        if 'trivia.html'in request:
            rvalue = trivhtml()
            
        elif 'trivia.css' in request:
            rvalue = trivcss()

    return rvalue


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
