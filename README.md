# Proj2 - pageserver
-------------

#Author:
--------
-Austin Mello

#Contact Information
--------------------
- Email: amello3@uoregon.edu

- Phone: 530-276-1662

- Zoom: amello3@uoregon.edu

#Description:
-------------
- Simple Web server using docker and flask. 
- Runs a Dockerfile that builds the Flask webserver.
- Takes an input from the user and outputs accordingly.
